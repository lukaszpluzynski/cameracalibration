import numpy as np
import cv2 as cv
from colorama import Fore
import urllib.request


class Camera:

    intrinsic_matrix = []

    def __init__(self,path):
        self.path = path


    def calibrateWebCam(self):

        def initChessBoardPattern(_rows,_cols):
            _rows_cols = (_rows, _cols)

            _objp = np.zeros((_rows * _cols, 3), np.float32)
            _objp [:, :2] = np.mgrid [0:_rows, 0:_cols].T.reshape(-1, 2)
            return _rows_cols, _objp

        print(Fore.YELLOW+'INITIALIZING CALIBRATION'+Fore.RESET)
        # INIT CRITERIA
        criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.01)

        # INIT CHESSBORD
        rows_cols, objp = initChessBoardPattern(5,8)

        objpoints = []
        imgpoints = []

        cap =cv.VideoCapture(self.path)
        idx = 0
        while(cap.isOpened()):
        # while(cap.get(cv.CAP_PROP_POS_FRAMES)!=cap.get(cv.CAP_PROP_FRAME_COUNT)):
            ret, img = cap.read()
            img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
            cv.flip(img,1,img)

            ret, corners = cv.findChessboardCorners(img, rows_cols, None)

            if ret == True:
                objpoints.append(objp)
                cv.cornerSubPix(img, corners, (11, 11), (-1, -1), criteria)
                imgpoints.append(corners)
                print(Fore.BLUE+'FRAME:'+Fore.RESET,'%06d.jpg' % cap.get(cv.CAP_PROP_POS_FRAMES),Fore.GREEN+'  DONE '+Fore.RESET)
                img = cv.cvtColor(img, cv.COLOR_GRAY2BGR)
                cv.putText(img,'DONE', (50,50), cv.FONT_HERSHEY_SIMPLEX, 1, (0,255,0),3,8)
                cv.drawChessboardCorners(img, rows_cols, corners, ret)
                if idx > 10:
                    break
                idx += 1
            else:
                print(Fore.BLUE+'FRAME:'+Fore.RESET,'%06d.png' % cap.get(cv.CAP_PROP_POS_FRAMES),Fore.RED+'  FAIL '+Fore.RESET)
                img = cv.cvtColor(img, cv.COLOR_GRAY2BGR)
                cv.putText(img,'FAIL', (50,50), cv.FONT_HERSHEY_SIMPLEX, 1, (0,0,255),3,8)
            cv.imshow('Calibration', img)
            if cv.waitKey(1000) == ord('b'):
                break
        cv.destroyAllWindows()

        try:
            img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
            ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(objpoints, imgpoints, img.shape [::-1], None, None)
            np.savez("intrinsic camera_params.npz", ret=ret, mtx=mtx, dist=dist, rvecs=rvecs, tvecs=tvecs)
            Camera.intrinsic_matrix = mtx

            print(f'CAM_MTX\n {mtx}')

            # ERROR
            mean_error = 0
            for i in range(len(objpoints)):
                imgpoints2, _ = cv.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, dist)
                error = cv.norm(imgpoints[i], imgpoints2, cv.NORM_L2)/len(imgpoints2)
                mean_error += error
            print( "Total error: {}".format(mean_error/len(objpoints)) )

        except:
            print ("Failed getting cv2.calibrateCamera")
            pass

    def estimate(self):

        def Cube(img, corners, imgpts):

            imgpts = np.int32(imgpts).reshape(-1, 2)

            # draw ground floor in green
            img = cv.drawContours(img,
                                  [imgpts [:4]],
                                  -1,
                                  (0, 255, 0),
                                  -3)
            # draw pillars in blue color
            for i, j in zip(range(4), range(4, 8)):
                img = cv.line(img,
                              tuple(imgpts [i]),
                              tuple(imgpts [j]),
                              (255),
                              3)
            # draw top layer in red color
            img = cv.drawContours(img,
                                  [imgpts [4:]],
                                  -1,
                                  (0, 0, 255),
                                  3)
            return img
        def initChessBoardPattern(_rows,_cols):
            _rows_cols = (_rows, _cols)

            _objp = np.zeros((_rows * _cols, 3), np.float32)
            _objp [:, :2] = np.mgrid [0:_rows, 0:_cols].T.reshape(-1, 2)
            return _rows_cols, _objp


        # LOAD DATA
        with np.load('intrinsic camera_params.npz') as X:
            mtx, dist, _, _ = [X [i] for i in ('mtx', 'dist', 'rvecs', 'tvecs')]

        # INIT CRITERIA
        criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.01)
        # INIT CHESSBORD
        rows_cols, objp = initChessBoardPattern(5,8)

        axis = np.float32([[0, 0, 0], [0, 3, 0], [3, 3, 0], [3, 0, 0],
                           [0, 0, -3], [0, 3, -3], [3, 3, -3], [3, 0, -3]])

        cap = cv.VideoCapture(self.path)

        while (True):
            ret, img = cap.read()
            cv.flip(img,1,img)

            gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
            ret, corners = cv.findChessboardCorners(gray, rows_cols, None)
            if ret == True:
                corners2 = cv.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)

                # Find the rotation and translation vectors.
                ret, rvecs, tvecs = cv.solvePnP(objp, corners2, mtx, dist)

                # project 3D points to image plane
                imgpts, jac = cv.projectPoints(axis, rvecs, tvecs, mtx, dist)
                img = Cube(img, corners2, imgpts)

                if cv.waitKey(1) == ord('q'):
                    quit()
            cv.imshow('img', img)

        cv.destroyAllWindows()

    def calibrateAndroidWebCam(self):

        def initChessBoardPattern(_rows,_cols):
            _rows_cols = (_rows, _cols)

            _objp = np.zeros((_rows * _cols, 3), np.float32)
            _objp [:, :2] = np.mgrid [0:_rows, 0:_cols].T.reshape(-1, 2)
            return _rows_cols, _objp

        print(Fore.YELLOW+'INITIALIZING CALIBRATION'+Fore.RESET)
        # INIT CRITERIA
        criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.01)

        # INIT CHESSBORD
        rows_cols, objp = initChessBoardPattern(5,8)

        objpoints = []
        imgpoints = []

        idx=0
        idx1=0
        while True:
            imgResp=urllib.request.urlopen(self.path)
            imgNp=np.array(bytearray(imgResp.read()),dtype=np.uint8)
            img=cv.imdecode(imgNp,-1)
            img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

            ret, corners = cv.findChessboardCorners(img, rows_cols, None)
            if ret == True:
                objpoints.append(objp)
                cv.cornerSubPix(img, corners, (11, 11), (-1, -1), criteria)
                imgpoints.append(corners)
                print(Fore.BLUE+'FRAME:'+Fore.RESET,'%06d.jpg' % idx1,Fore.GREEN+'  DONE '+Fore.RESET)
                img = cv.cvtColor(img, cv.COLOR_GRAY2BGR)
                cv.putText(img,'DONE', (50,50), cv.FONT_HERSHEY_SIMPLEX, 1, (0,255,0),3,8)
                cv.drawChessboardCorners(img, rows_cols, corners, ret)
                if idx > 10:
                    break
                idx += 1
            else:
                print(Fore.BLUE+'FRAME:'+Fore.RESET,'%06d.png' % idx1,Fore.RED+'  FAIL '+Fore.RESET)
                img = cv.cvtColor(img, cv.COLOR_GRAY2BGR)
                cv.putText(img,'FAIL', (50,50), cv.FONT_HERSHEY_SIMPLEX, 1, (0,0,255),3,8)
            cv.imshow('Calibration', img)
            idx1 += 1
            if cv.waitKey(1000) == ord('b'):
                break
        cv.destroyAllWindows()

        try:
            img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
            ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(objpoints, imgpoints, img.shape [::-1], None, None)
            np.savez("intrinsic camera_params.npz", ret=ret, mtx=mtx, dist=dist, rvecs=rvecs, tvecs=tvecs)
            Camera.intrinsic_matrix = mtx

            print(f'CAM_MTX\n {mtx}')

            # ERROR
            mean_error = 0
            for i in range(len(objpoints)):
                imgpoints2, _ = cv.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, dist)
                error = cv.norm(imgpoints[i], imgpoints2, cv.NORM_L2)/len(imgpoints2)
                mean_error += error
            print( "Total error: {}".format(mean_error/len(objpoints)) )

        except:
            print ("Failed getting cv2.calibrateCamera")
            pass

    def estimateAndroid(self):

        def Cube(img, corners, imgpts):

            imgpts = np.int32(imgpts).reshape(-1, 2)

            # draw ground floor in green
            img = cv.drawContours(img,
                                  [imgpts [:4]],
                                  -1,
                                  (0, 255, 0),
                                  -3)
            # draw pillars in blue color
            for i, j in zip(range(4), range(4, 8)):
                img = cv.line(img,
                              tuple(imgpts [i]),
                              tuple(imgpts [j]),
                              (255),
                              3)
            # draw top layer in red color
            img = cv.drawContours(img,
                                  [imgpts [4:]],
                                  -1,
                                  (0, 0, 255),
                                  3)
            return img
        def initChessBoardPattern(_rows,_cols):
            _rows_cols = (_rows, _cols)

            _objp = np.zeros((_rows * _cols, 3), np.float32)
            _objp [:, :2] = np.mgrid [0:_rows, 0:_cols].T.reshape(-1, 2)
            return _rows_cols, _objp


        # LOAD DATA
        with np.load('intrinsic camera_params.npz') as X:
            mtx, dist, _, _ = [X [i] for i in ('mtx', 'dist', 'rvecs', 'tvecs')]

        # INIT CRITERIA
        criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.01)
        # INIT CHESSBORD
        rows_cols, objp = initChessBoardPattern(5,8)

        axis = np.float32([[0, 0, 0], [0, 3, 0], [3, 3, 0], [3, 0, 0],
                           [0, 0, -3], [0, 3, -3], [3, 3, -3], [3, 0, -3]])

        cap = cv.VideoCapture(self.path)

        while True:
            imgResp=urllib.request.urlopen(self.path)
            imgNp=np.array(bytearray(imgResp.read()),dtype=np.uint8)
            img=cv.imdecode(imgNp,-1)

            gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
            ret, corners = cv.findChessboardCorners(gray, rows_cols, None)
            if ret == True:
                corners2 = cv.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)

                # Find the rotation and translation vectors.
                ret, rvecs, tvecs = cv.solvePnP(objp, corners2, mtx, dist)

                # project 3D points to image plane
                imgpts, jac = cv.projectPoints(axis, rvecs, tvecs, mtx, dist)
                img = Cube(img, corners2, imgpts)

                if cv.waitKey(1) == ord('q'):
                    quit()
            cv.imshow('img', img)

        cv.destroyAllWindows()
