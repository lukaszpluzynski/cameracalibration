import numpy as np
import cv2 as cv
from pathlib import Path
from colorama import Fore



criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)

rows = 5
cols = 8
rows_and_cols = (rows, cols)

objp = np.zeros((rows * cols, 3), np.float32)
objp[:,:2] = np.mgrid[0:rows, 0:cols].T.reshape(-1, 2)


objpoints = []
imgpoints = []


mypath = Path('/Users/lukaszpluzynski/Desktop/folder bez nazwy/Calibration_set_photos')

for idx,fname in enumerate(sorted(mypath.glob('*.jpg'))):
    # if idx > 10:
    #      break
    img = cv.imread(str(fname))
    img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

    ret, corners = cv.findChessboardCorners(img, rows_and_cols, None)

    if ret == True:
        print(Fore.BLUE+'FRAME:'+Fore.RESET,f'{str(fname)}',Fore.GREEN+f'   DONE '+Fore.RESET)
        objpoints.append(objp)
        cv.cornerSubPix(img,corners, (11,11), (-1,-1), criteria)
        imgpoints.append(corners)

        cv.drawChessboardCorners(img, rows_and_cols, corners, ret)
    else:
        print(Fore.BLUE+'FRAME:'+Fore.RESET,f'{str(fname)}',Fore.RED+f'   FAIL '+Fore.RESET)
    cv.imshow('Calibration', img)
    if cv.waitKey(10000) == ord('q'):
            quit()

try:
    ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(objpoints, imgpoints, img.shape [::-1], None, None)
    np.savez("intrinsic camera_params.npz", ret=ret, mtx=mtx, dist=dist, rvecs=rvecs, tvecs=tvecs)
    print(f'CAM_MTX\n {mtx}')
except:
    print ("Failed getting cv2.calibrateCamera")
    pass


mean_error = 0
for i in range(len(objpoints)):
    imgpoints2, _ = cv.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, dist)
    error = cv.norm(imgpoints[i], imgpoints2, cv.NORM_L2)/len(imgpoints2)
    mean_error += error

print( "Total error: {}".format(mean_error/len(objpoints)) )

cv.destroyAllWindows()
