import numpy as np
import cv2 as cv


def Cube(img, corners, imgpts):

    imgpts = np.int32(imgpts).reshape(-1, 2)

    # draw ground floor in green
    img = cv.drawContours(img,
                          [imgpts [:4]],
                          -1,
                          (0, 255, 0),
                          -3)
    # draw pillars in blue color
    for i, j in zip(range(4), range(4, 8)):
        img = cv.line(img,
                      tuple(imgpts [i]),
                      tuple(imgpts [j]),
                      (255),
                      3)
    # draw top layer in red color
    img = cv.drawContours(img,
                          [imgpts [4:]],
                          -1,
                          (0, 0, 255),
                          3)
    return img



# Load previously saved data
with np.load('intrinsic camera_params.npz') as X:
    mtx, dist, _, _ = [X [i] for i in ('mtx', 'dist', 'rvecs', 'tvecs')]



criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.01)

rows = 5
cols = 8
rows_and_cols = (rows, cols)

objp = np.zeros((rows * cols, 3), np.float32)
objp [:, :2] = np.mgrid [0:rows, 0:cols].T.reshape(-1, 2)


axis = np.float32([[0, 0, 0], [0, 3, 0], [3, 3, 0], [3, 0, 0],
                   [0, 0, -3], [0, 3, -3], [3, 3, -3], [3, 0, -3]])


# cap = cv.VideoCapture('/Users/lukaszpluzynski/Desktop/folder bez nazwy/Movie_03.mov', 0)
cap = cv.VideoCapture(0)

while (True):
    ret, img = cap.read()
    cv.flip(img,1,img)

    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    ret, corners = cv.findChessboardCorners(gray, rows_and_cols, None)
    if ret == True:
        corners2 = cv.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)

        # Find the rotation and translation vectors.
        ret, rvecs, tvecs = cv.solvePnP(objp, corners2, mtx, dist)

        # project 3D points to image plane
        imgpts, jac = cv.projectPoints(axis, rvecs, tvecs, mtx, dist)
        img = Cube(img, corners2, imgpts)
        if cv.waitKey(1) == ord('q'):
            quit()
    cv.imshow('img', img)

cv.destroyAllWindows()
