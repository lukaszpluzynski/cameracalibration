import numpy as np
import cv2 as cv
from colorama import Fore

print(Fore.YELLOW+'INITIALIZING CALIBRATION'+Fore.RESET)
criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
rows = 5
cols = 8
rows_and_cols = (rows, cols)

objp = np.zeros((rows * cols, 3), np.float32)
objp[:,:2] = np.mgrid[0:rows, 0:cols].T.reshape(-1, 2)


objpoints = []
imgpoints = []


# cap =cv.VideoCapture('/Users/lukaszpluzynski/Desktop/folder bez nazwy/Calibration_set_photos/%06d.jpg',0)
cap =cv.VideoCapture(0)
idx = 0
while(cap.isOpened()):
# while(cap.get(cv.CAP_PROP_POS_FRAMES)!=cap.get(cv.CAP_PROP_FRAME_COUNT)):
    ret, img = cap.read()
    img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    cv.flip(img,1,img)

    ret, corners = cv.findChessboardCorners(img, rows_and_cols, None)

    if ret == True:
        objpoints.append(objp)
        cv.cornerSubPix(img, corners, (11, 11), (-1, -1), criteria)
        imgpoints.append(corners)
        print(Fore.BLUE+'FRAME:'+Fore.RESET,'%06d.jpg' % cap.get(cv.CAP_PROP_POS_FRAMES),Fore.GREEN+'  DONE '+Fore.RESET)
        img = cv.cvtColor(img, cv.COLOR_GRAY2BGR)
        cv.putText(img,'DONE', (50,50), cv.FONT_HERSHEY_SIMPLEX, 1, (0,255,0),3,8)
        cv.drawChessboardCorners(img, rows_and_cols, corners, ret)
        if idx > 20:
            break
        idx += 1
    else:
        print(Fore.BLUE+'FRAME:'+Fore.RESET,'%06d.png' % cap.get(cv.CAP_PROP_POS_FRAMES),Fore.RED+'  FAIL '+Fore.RESET)
        img = cv.cvtColor(img, cv.COLOR_GRAY2BGR)
        cv.putText(img,'FAIL', (50,50), cv.FONT_HERSHEY_SIMPLEX, 1, (0,0,255),3,8)
    cv.imshow('Calibration', img)
    if cv.waitKey(1000) == ord('b'):
        break
cv.destroyAllWindows()

try:
    img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(objpoints, imgpoints, img.shape [::-1], None, None)
    np.savez("intrinsic camera_params.npz", ret=ret, mtx=mtx, dist=dist, rvecs=rvecs, tvecs=tvecs)
    print(f'CAM_MTX\n {mtx}')

    # ERROR
    mean_error = 0
    for i in range(len(objpoints)):
        imgpoints2, _ = cv.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, dist)
        error = cv.norm(imgpoints[i], imgpoints2, cv.NORM_L2)/len(imgpoints2)
        mean_error += error
    print( "Total error: {}".format(mean_error/len(objpoints)) )

except:
    print ("Failed getting cv2.calibrateCamera")
    pass

